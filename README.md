# fitness_app

Fitness app is an app designed to make it easy to do your workouts.

### How to run desktop embedded?

1. `git clone https://github.com/google/flutter-desktop-embedding.git`
2. paste this repository as under the root of `example/` in the just cloned repo.
3. paste flutter framework in the same folder as `flutter-desktop-embedding`
4. Try run for example MacOS using xCode by opening the xCode project and clicking `run`

### How to run Android/iPhone

1. open `flutter_app` in Visual Studio Code (*Recommended) or Android Studio
2. connect your phone or run a emulator/simulator
3. open a dart file from within `lib/` hit F5

### Tested platforms
1. Android
2. MacOS
3. Ubuntu (Linux x86_64)

### What should work too

1. Windows?
2. iPhone?

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).
