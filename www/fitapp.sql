-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 07, 2018 at 09:54 AM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.12-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fitapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `exercise`
--

CREATE TABLE `exercise` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `exerciseData` varchar(4096) NOT NULL,
  `estimatedTime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exercise`
--

INSERT INTO `exercise` (`id`, `name`, `exerciseData`, `estimatedTime`) VALUES
(1, 'Push-ups', '{\"exercise\":[{\"amount\":45,\"workout\":\"pushups\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":30,\"workout\":\"pushups\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":30,\"workout\":\"pushups\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":30,\"workout\":\"pushups\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":45,\"workout\":\"pushups\"}]}', 300),
(2, 'Sit-ups', '{\"exercise\":[{\"amount\":45,\"workout\":\"situps\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":30,\"workout\":\"situps\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":30,\"workout\":\"situps\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":30,\"workout\":\"situps\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":45,\"workout\":\"situps\"}]}', 300),
(3, 'Pushups and Situps', '{\"exercise\":[{\"amount\":10,\"workout\":\"situps\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":30,\"workout\":\"pushups\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":30,\"workout\":\"situps\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":30,\"workout\":\"pushups\"},{\"amount\":30,\"workout\":\"rest\"},{\"amount\":45,\"workout\":\"situps\"}]}', 300);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `fullName` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `totalTime` int(11) NOT NULL,
  `totalSteps` int(11) NOT NULL,
  `goalsReached` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(4096) NOT NULL,
  `level` varchar(64) NOT NULL DEFAULT 'member'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `fullName`, `location`, `totalTime`, `totalSteps`, `goalsReached`, `email`, `password`, `level`) VALUES
(1, 'Menno van Leeuwen', 'Beverwijk', 13460, 9434, 34, 'menno@vleeuwen.me', '12345', 'member'),
(2, 'Dunccan Groenedijk', 'Ijmuiden', 7634, 3323, 32, 'dunccan', '12345', 'member'),
(3, 'Test Account', 'NGC 1052-df2', 345657635, 0, 0, 'a', 'a', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `exercise`
--
ALTER TABLE `exercise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `exercise`
--
ALTER TABLE `exercise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
