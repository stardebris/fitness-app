import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/services.dart' show rootBundle;

import 'tabController.dart';
import 'utils.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

List userData;

class LoginPageMain extends StatelessWidget
{    
    @override
    Widget build(BuildContext context) 
    {
        return new MaterialApp(
            title: 'Flutter Demo',
            home: new LoginPage(),
        );
    }
}

class LoginPage extends StatefulWidget 
{
    static String tag = 'login-page';
    @override
    _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> 
{
    TextEditingController emailField = new TextEditingController();
    TextEditingController passField = new TextEditingController();

    String msg = '';

    Future<List> _login() async 
    {
        final response = await http.post("https://stardebris.net/login.php", body: {
            "email": emailField.text,
            "password": passField.text,
        });

        var datauser = json.decode(response.body);

        if(datauser.length==0)
        {
            setState(() {
                msg="Failed to log you in";
            });
        }
        else
        {
            if(datauser[0]['level']=='admin')
            {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MainTabController(userData)));
            }
            else if(datauser[0]['level']=='member')
            {
                Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MainTabController(userData)));
            }

            setState(() {
                userData = datauser;
            });
        }

        return datauser;
    }

    @override
    Widget build(BuildContext context) {
    final logo = Hero(
        tag: 'hero',
        child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 72.0,
        child: Image.asset('assets/avatar.png'),
        ),
    );


    final email = TextField(
        controller: emailField,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),
    );

    final password = TextField(
        controller: passField,
        autofocus: false,
        obscureText: true,
        decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
        ),
    );

    final loginButton = Padding(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        child: MaterialButton(
            minWidth: 200.0,
            height: 42.0,
            onPressed: () {
                _login();
            },
            color: Colors.lightBlueAccent,
            child: Text('Log In', style: TextStyle(color: Colors.white)),
        ),
    );

    final socialLoginButtons = 
        Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
            new FloatingActionButton(
                onPressed: () {
                    setState(() {
                        debugPrint("FB login tapped...");
                    });
                },
                child: new ConstrainedBox(
                    constraints: new BoxConstraints.expand(),
                    child: Image(image: AssetImage("assets/facebook.png"), fit: BoxFit.cover, gaplessPlayback: true),
                ),
            ),
            new FloatingActionButton(
                onPressed: () {
                    setState(() {
                        debugPrint("Google login tapped...");
                    });
                },
                child: new ConstrainedBox(
                    constraints: new BoxConstraints.expand(),
                    child: Image(image: AssetImage("assets/google.png"), fit: BoxFit.cover, gaplessPlayback: true),
                ),
            ),
            new FloatingActionButton(
                onPressed: () {
                    setState(() {
                        debugPrint("WeChat login tapped...");
                    });
                },
                child: new ConstrainedBox(
                    constraints: new BoxConstraints.expand(),
                    child: Image(image: AssetImage("assets/wechat.png"), fit: BoxFit.cover, gaplessPlayback: true),
                ),
            ),
            new FloatingActionButton(
                onPressed: () {
                    setState(() {
                        debugPrint("LINE login tapped...");
                    });
                },
                child: new ConstrainedBox(
                    constraints: new BoxConstraints.expand(),
                    child: Image(image: AssetImage("assets/line.png"), fit: BoxFit.cover, gaplessPlayback: true),
                ),
            )
        ]);

    final forgotLabelNewAccount = 
    Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
            FlatButton(
                child: Text(
                    'Forgot password?',
                    style: TextStyle(color: Colors.black54),
                ),
                onPressed: () {},
            ),
            FlatButton(
                child: Text(
                    'Sign up',
                    style: TextStyle(color: Colors.black54),
                ),
                onPressed: () {},
            ),
        ]);

    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
        child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            email,
            SizedBox(height: 8.0),
            password,
            SizedBox(height: 8.0),
            loginButton,
            SizedBox(height: 8.0),
            new Text(msg),
            socialLoginButtons,
            SizedBox(height: 16.0),
            forgotLabelNewAccount
            ],
        ),
        ),
    );
    }
}