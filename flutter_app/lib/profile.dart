class Profile 
{
	String firstName;
	String lastName;
	String location;

	int totalTime;
	int reachedGoals;
	int totalSteps;

	String get fullName => "$firstName $lastName";
	String get totalTimeString => _timeCount(totalTime);
	String get reachedGoalsString => _abbreviatedCount(reachedGoals);
	String get totalStepsString => _abbreviatedCount(totalSteps);

	String _abbreviatedCount(int num) 
	{
		if (num < 1000) return "$num";

		if (num >= 1000 && num < 1000000)
		{
			String s = (num / 1000).toStringAsFixed(1);

			if (s.endsWith(".0")) 
			{
				int idx = s.indexOf(".0");
				s = s.substring(0, idx);
			}

			return "${s}K";
		} 
		else if (num >= 1000000 && num < 1000000000) 
		{
			String s = (num / 1000000).toStringAsFixed(1);

			if (s.endsWith(".0")) 
			{
				int idx = s.indexOf(".0");
				s = s.substring(0, idx);
			}

			return "${s}M";
		}
		return "";
	}

	String _timeCount(int num)
	{
		return new Duration(seconds: num).toString().substring(0,7);
	}
}

Profile parseUserData(List userData)
{
    return new Profile()
	..firstName = userData[0]["fullName"].toString().substring(0, userData[0]["fullName"].toString().indexOf(' '))
	..lastName = userData[0]["fullName"].toString().substring(userData[0]["fullName"].toString().indexOf(' '))
	..location = userData[0]["location"].toString()
	..totalTime = int.parse(userData[0]["totalTime"])
	..reachedGoals = int.parse(userData[0]["goalsReached"])
	..totalSteps = int.parse(userData[0]["totalSteps"]);
}

Profile getMockupProfile() 
{
	return new Profile()
	..firstName = "Dunccan"
	..lastName = "Groenendijk"
	..location = "IJmuiden"
	..totalTime = 355923
	..reachedGoals = 924
	..totalSteps = 1700;
}