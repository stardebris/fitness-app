import 'package:flutter/material.dart';
import 'package:fitness_app/landingPage.dart';
import 'package:intl/intl.dart';

class Statistics extends StatefulWidget 
{
    const Statistics({ Key key }) : super(key: key);

    @override
    _StatisticsState createState() => new _StatisticsState();
}

class _StatisticsState extends State<Statistics> with SingleTickerProviderStateMixin 
{
    final List<Tab> myTabs = <Tab>[
        new Tab(text: 'MONTH-01-2019'),
        new Tab(text: 'MONTH-02-2019'),
        new Tab(text: 'MONTH-03-2019'),
    ];

    final List<Widget> tabContainers = <Widget>[
        new Text("01-2019"),
        new Text("02-2019"),
        new Text("03-2019")
    ];

    TabController _tabController;
    DateTime currentMonthYear;

    @override
    void initState() 
    {
        super.initState();
        _tabController = new TabController(vsync: this, length: myTabs.length);
    }

    @override
    void dispose() 
    {
        _tabController.dispose();
        super.dispose();
    }

    @override
    Widget build(BuildContext context) 
    {
        currentMonthYear = DateTime.now();
        var formatter = new DateFormat('MMMM');

        return new Scaffold(
            //Let's display 3 months Example: January, February(Active) and March
            //The moment we swap to January as active we can unload March and load December
             body: new Stack(children: [
                new Padding(padding: new EdgeInsets.only(top: 42.0),
                    child: new TabBarView(
                            controller: _tabController,
                            children: tabContainers,
                        ),
                ),
                new Row(children: <Widget>[
                    new FlatButton(child: new Icon(Icons.chevron_left), onPressed: () {

                    },),
                    new Padding(padding: EdgeInsets.all(8.0),
                        child: new Column(children: [
                                new Text("Month"),
                                new Row(children: <Widget>[
                                    new Text(formatter.format(currentMonthYear)),
                                    new Padding(padding: new EdgeInsets.all(2.0)),
                                    new Text(currentMonthYear.year.toString())
                                ],)
                            ]),
                    ),
                    new FlatButton(child: new Icon(Icons.chevron_right), onPressed: () {

                    },),
                ],
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,),
             ])
        );
    }
}