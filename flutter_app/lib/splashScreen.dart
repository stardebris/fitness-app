import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'dart:async';

import 'Utils.dart';
import 'landingPage.dart';
import 'loginPage.dart';

class SplashScreenAnimation extends StatefulWidget 
{
    @override
    SplashScreenAnimationState createState() => SplashScreenAnimationState();
}

class SplashScreenAnimationState extends State<SplashScreenAnimation> with SingleTickerProviderStateMixin 
{
    Animation animation;
    AnimationController animationController;

    @override
    void initState() 
    {
        super.initState();

        animationController = AnimationController(duration: Duration(milliseconds: 2000), vsync: this);
        animation = Tween(begin: 2000.0, end: 0.0).animate(animationController);

        animation.addStatusListener((status) 
        {
            if (status == AnimationStatus.completed) 
            {
                Utils.strFileExists("disableFirstTime.json").then((b) {
                    if (b)
                    {
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LandingPage())); //TODO: Disable me and enable lines below>>
                        //debugPrint("We already completed the introduction before, skipping it...");
                        //Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LoginPageMain()));
                    }
                    else
                    {
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LandingPage()));
                    }
                });
            } 
            else if (status == AnimationStatus.dismissed) 
            {
                animationController.forward();
            }
        });
        
        animationController.forward();
    }

    @override
    Widget build(BuildContext context) 
    {
        return LogoAnimation(
            animation: animation,
        );
    }

    @override
    void dispose() 
    {
        animationController.dispose();
        super.dispose();
    }
}

class LogoAnimation extends AnimatedWidget 
{
    LogoAnimation({Key key, Animation animation})
        : super(key: key, listenable: animation);

    @override
    Widget build(BuildContext context) 
    {
        Animation animation = listenable;

        return Scaffold(
                body: Stack(
                fit: StackFit.expand,
                    children: <Widget>[
                    Container(
                        decoration: BoxDecoration(color: Colors.blue),
                    ),
                    Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                        Expanded(
                            flex: 2,
                            child: Container(
                                height: animation.value,
                                width: animation.value,
                                child: CircleAvatar(
                                    backgroundColor: Colors.white,
                                    radius: 200.0,
                                ),
                            ),
                        )
                        ],
                    )
                ],
            ),
        );
    }
}
