import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'tabController.dart';

class OutroPage extends StatefulWidget {
  	final List userData;
	OutroPage(this.userData);

  @override
  _OutroPageState createState() => new _OutroPageState(userData);
}

class _OutroPageState extends State<OutroPage>
    with SingleTickerProviderStateMixin {

	final List userData;
	_OutroPageState(this.userData);

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'End Page',
      home: Scaffold(
        appBar: AppBar(
          leading: BackButton(color: Colors.white),
          title: Text('End Page'),
        ),
        body: new Container(
          margin: EdgeInsets.all(16.0),
          child: new Column(
            //maak
            children: <Widget>[
              //centreerd de card
              Center(
                //Maakt een card aan
                child: Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Text(
                          'Good job!',
                          style: TextStyle(
                            fontSize: 24.0,
                          ),
                        ),
                      ),
                      Image.asset('assets/medal.jpg'),
                      Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Text(
                          'It took you {0} seconds to complet the exercise!', //Paste exercise time here
                          style: TextStyle(
                            fontSize: 16.0,
                          ),
                        ),
                      ),
					  Padding(padding: EdgeInsets.all(8), child:
						RaisedButton(
							child: const Text('Back to menu!',
								style: TextStyle(color: Colors.white)),
							color: Theme.of(context).accentColor,
							elevation: 4.0,
							onPressed: () {
                Navigator.push(
                          context, MaterialPageRoute(builder: (context) => MainTabController(userData)));
							},
						)
					  ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
